var BSTree = function () {};

BSTree.prototype.init = () => {};//++++++
BSTree.prototype.add = () => {}; // =>>+++
BSTree.prototype.getHeight = () => {}; // =>>int
BSTree.prototype.getWidth = () => {}; // =>>int
BSTree.prototype.remove = (value) => {}; // =>>int
BSTree.prototype.find = (value) => {}; // =>>Node
BSTree.prototype.toArray = () => {}; // =>>Arr
BSTree.prototype.clear = () => {};//++++
BSTree.prototype.getNodes = () => {}; // =>>int
BSTree.prototype.getLeaves = () => {}; // =>>int
BSTree.prototype.print = () => {}; // =>>String
BSTree.prototype.reverse = () => {};

function BSTree(){
    this.root = null;
}
var Node = function (value) {
    this.value = value;
    this.right = null;
    this.left = null;
};

BSTree.prototype.clear = function (array) {
    this.root = null;
};

BSTree.prototype.init = function (array) {
    this.clear();
    for (var i = 0; i < array.length; i++) {
        this.add(array[i]);
    }
};

BSTree.prototype.add = function(value) {
    if (value === null) {
        return;
    }
    this.root = this.addNode(this.root, value);
};

BSTree.prototype.addNode = function(node, value) {
    if (node === null) {
        node = new Node(value);
    }
    else if (value < node.value) {
        node.left = this.addNode(node.left, value);
    } else {
        node.right = this.addNode(node.right, value);
    }
    return node;
};

BSTree.prototype.find = function (value) {
    if (this.root.left !== null || this.root.right !== null) {
        if (this.root.value >= value) {
            if (this.root.value == value) {
                return this.root
            }
            this.root = this.root.left;
            this.find(value)
        }  else if (this.root.value < value){
            this.root = this.root.right;
            this.find(value)
        }
    }
    return this.root
};

BSTree.prototype.getHeight = function (node=this.root) {
    if(node===null) {
        return -1;
    }
    let left = this.getHeight(node.left);
    let right = this.getHeight(node.right);
    if(left > right) {
        return left + 1;
    }else {
        return right + 1;
    }
};




var tree = new BSTree();
tree.init([8,3,10,1,6,14,4,7,13,25,16]);

console.log(tree);
console.log(tree.getHeight());
console.log(tree.getWidth());







