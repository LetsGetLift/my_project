var path = require("path");
var {CleanWebpackPlugin} = require('clean-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: {
        index: "./src/index",
    },
    output: {
        filename: "index.[hash:8].js",
        path: path.resolve(__dirname, "dist"),
    },
    plugins: [
        new HtmlWebpackPlugin({
        filename: 'index.html',
        template: 'public/index.html',
    }),

        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin(),
    ],
    watch: true,
    devServer: {
        contentBase: './dist',
    },
    module: {

        rules: [
            {
                test: /\.less$/,
                exclude: /node_modules/,
                loader: 'style-loader!css-loader!less-loader'
            },

            {
                test: /\.(png|jpg|gif|svg|gif)$/,
                use: ['file-loader'],
            },

        ],
    },

};