export {
    arrayForConvert,
    languageTranslate
};
let arrayForConvert = {
    mile: 1609.34,
    verst: 1066.8,
    yard: 0.9144,
    foot: 0.3048,
    meter: 1,
    миля: 1609.34,
    верста: 1066.8,
    ярд: 0.9144,
    фут: 0.3048,
    метр: 1,
    متر: 1,
    الفرست: 1066.8,
    ساحة: 0.9144,
    قدم: 0.3048,
    ميل: 1609.34
};
let languageTranslate = {
    eng: {
        select: {
            meter: "meter",
            verst: "verst",
            yard: "yard",
            foot: "foot",
            mile: "mile"
        },
        text: {
            h1Text: "Converter",
            h2Text: "Settings",
            titleLabel: "Converter",
            labelInputDigits: "Enter value",
            labelInputResult: "Result",
            convert: "Convert",
        },
        message: {
            positive: "Enter a positive number",
            plus: "No need to write +",
            minus: "Unable to count negative length",
            short: "The number should be shorter!",
        },
        placeholder: {
            inputDigits: "Write digit",
            inputResult: "Result"

        }


    },
    ru: {
        select: {
            meter: "метр",
            verst: "верста",
            yard: "ярд",
            foot: "фут",
            mile: "миля"
        },
        text: {
            h1Text: "Конвертер",
            h2Text: "Настройки",
            titleLabel: "Конвертер",
            labelInputDigits: "Введите значение",
            labelInputResult: "Результат",
            convert: "Конвертировать",

        },
        message: {
            positive: "Введите положительное чиcло",
            plus: "Не нужно писать +",
            minus: "Невозможно посчитать отрицательную длину",
            short: "Число должно быть короче!",
        },
        placeholder: {
            inputDigits: "Введите число",
            inputResult: "Результат"
        }
    },
    ar: {
        select: {
            meter: "متر",
            verst: "الفرست",
            yard: "ساحة",
            foot: "قدم",
            mile: "ميل"
        },
        text: {
            h1Text: "محول",
            h2Text: "إعدادات",
            titleLabel: "محول",
            labelInputDigits: "أدخل القيمة",
            labelInputResult: "يؤدي",
            convert: "تحويل",
            settingsLabel: "إعدادات",
        },
        message: {
            positive: "أدخل رقم موجب",
            plus: "لا حاجة لكتابة +",
            minus: "غير قادر على حساب الطول السلبي",
            short: "يجب أن يكون الرقم أقصر!",
        },
        placeholder: {
            inputDigits: "أدخل الرقم",
            inputResult: "يؤدي"
        }
    },

};
