export {getElement};
function getElement() {
    let inputDigits = document.querySelector("#inputDigits");
    let selectFrom = document.querySelector("#selectFrom");
    let selectTo = document.querySelector("#selectTo");
    let inputResult = document.querySelector("#inputResult");
    let settingModal = document.querySelector(".settings-modal");
    let selectLanguage=document.querySelector("#selectLanguage")
    return { inputDigits, selectFrom, selectTo, inputResult, selectLanguage, settingModal};
};