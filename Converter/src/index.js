import {arrayForConvert, languageTranslate} from "./const";
import "./style/index.less";
import {saveSessionStorage, loadSessionStorage} from "./localStorage";


let inputDigits = document.querySelector("#inputDigits");
let selectFrom = document.querySelector("#selectFrom");
let selectTo = document.querySelector("#selectTo");
let inputResult = document.querySelector("#inputResult");
let settingModal = document.querySelector(".settings-modal");
let settings = document.querySelector(".settings");
let convert = document.querySelector("#convert");
let selectLanguage = document.querySelector("#selectLanguage");
document.querySelector(".settings-button").addEventListener("click", openSettingModal);
document.querySelector("#convert").addEventListener("click", getConvert);
inputResult.setAttribute("disabled", "disabled");
selectLanguage.addEventListener("click", changeLang);

inputDigits.oninput = function () {
    convert.disabled = this.value.length <= 0;
    this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
};



function saveSession() {
    if (sessionStorage.length != 0) {
        loadSessionStorage();
    }
    changeLang();
};
saveSession();

loadSessionStorage();

function openSettingModal() {
    settingModal.classList.toggle("active");
}

document.getElementById("btn-close-modal").addEventListener("click", () => {
    settingModal.classList.remove("active");
});

function getConvert() {
    let optionsValueFrom = selectFrom.options[selectFrom.selectedIndex].text;
    let optionsValueTo = selectTo.options[selectTo.selectedIndex].text;
    convertResult(optionsValueFrom, optionsValueTo);
    saveSessionStorage();
}

function convertToMeter(optionsValueFrom) {
    let meterConvert = inputDigits.value;
    return meterConvert * arrayForConvert[optionsValueFrom];
}

function convertResult(optionsValueFrom, optionsValueTo) {
    let result = convertToMeter(optionsValueFrom);
    inputResult.value = result / arrayForConvert[optionsValueTo];
}

function changeLang() {
    let lang = selectLanguage.value;
    let wrapper = document.querySelector(".wrapper-main");
    let settingsButton = document.querySelector(".settings-button");
    let btnCloseModal=document.getElementById("#btn-close-modal");
    if (lang === "ar") {
        wrapper.setAttribute("dir","rtl");
        settingsButton.setAttribute("dir","ltr");
        saveSessionStorage();
    } else {
        wrapper.setAttribute("dir","ltr");
        settingsButton.setAttribute("dir","rtl");
        saveSessionStorage();
    }
    changeSelect(languageTranslate[lang].select, "selectFrom");
    changeSelect(languageTranslate[lang].select, "selectTo");
    changeText(languageTranslate[lang].text);

}

function changeSelect(obj, selector) {
    let index = 0;
    let elementSelect = document.getElementById(selector);
    for (const key in obj) {
        elementSelect.options[index].innerHTML = obj[key];
        index++;
    }

}

function changeText(obj) {
    for (const key in obj) {
        document.getElementById(key).innerHTML = obj[key];
    }
}



























