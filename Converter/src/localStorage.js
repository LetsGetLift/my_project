export {saveSessionStorage,loadSessionStorage};

let inputDigits = document.querySelector("#inputDigits");
let selectFrom = document.querySelector("#selectFrom");
let selectTo = document.querySelector("#selectTo");
let inputResult = document.querySelector("#inputResult");
let selectLanguage = document.querySelector("#selectLanguage");

function saveSessionStorage(element) {
    sessionStorage.setItem('inputDigits', inputDigits.value);
    sessionStorage.setItem('inputResult', inputResult.value);
    sessionStorage.setItem('selectFrom', selectFrom.value);
    sessionStorage.setItem('selectTo', selectTo.value);
    sessionStorage.setItem('selectLanguage', selectLanguage.value);
}
function loadSessionStorage() {
    inputDigits.value = sessionStorage.getItem('inputDigits');
    inputResult.value = sessionStorage.getItem('inputResult');
    selectFrom.value = sessionStorage.getItem('selectFrom');
    selectTo.value = sessionStorage.getItem('selectTo');
    selectLanguage.value = sessionStorage.getItem('selectLanguage');
}
