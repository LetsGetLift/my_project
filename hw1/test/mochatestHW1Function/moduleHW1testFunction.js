function getDayOfWeek(dayNumber) {
		switch (dayNumber) {
			case 1:
				return "Monday";
			case 2:
				return "Tuesday";
			case 3:
				return "Wednesday";
			case 4:
				return "Thursday";
			case 5:
				return "Friday";
			case 6:
				return "Saturday";
			case 7:
				return "Sunday";
			default:
				return "Error";
		}
	}

function getDistanceBetweenXandY(pointAx,pointAy,pointBx,pointBy){

    var calcDistance=Math.sqrt(((pointBx-pointAx))**2+((pointBy-pointAy))**2);

    return calcDistance;
}

function getTextFromNumber(number) {

	var num = [, "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight",
		"Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen",
		"Seventeen", "Eighteen", "Nineteen"];
	var tens = ["Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"];
	if (number < 20) {
		return num[number];
	}
	var modulo = number % 10;
	if (number < 100) {
		return tens[Math.floor(number / 10) - 2] + (modulo ? " " + num[modulo] : "");
	}
	if (number < 1000) {
		var index = Math.floor(number / 100);
		return `${num[index]} Hundreds ${number % 100 === 0 ? "" : getTextFromNumber(number % 100)}`.trim();
	}
}

function getNumberFromText(inputText) {

	if(typeof (inputText)!=="string"){
		return "Error";
	}

	var newText = inputText.toLowerCase().split(" ");

	var num = {
		null: 0, 
		zero: 0,
		one: 1, 
		two: 2,
		three: 3,
		four: 4, 
		five: 5, 
		six: 6, 
		seven: 7, 
		eight: 8, 
		nine: 9,
		ten: 10, 
		eleven: 11, 
		twelve: 12, 
		thirteen: 13, 
		fourteen: 14, 
		fifteen: 15, 
		sixteen: 16,
		seventeen: 17, 
		eighteen: 18, 
		nineteen: 19, 
		twenty: 20,
		thirty: 30, 
		forty: 40, 
		fifty: 50,
		sixty: 60, 
		seventy: 70, 
		eighty: 80, 
		ninety: 90,
		hundred: 0
};

	var result;
	for(var i=0; i<newText.length;i++){
		if(newText.length===1){
			result=num[newText];
		}
		if(newText.length===2 && newText[1] !== "hundred"){
			result = num[newText[0]]+num[newText[1]];
		}
		if(newText.length===2 && newText[1] === "hundred"){
			result = num[newText[0]]*100;
		}
		if(newText.length===3){
			result = (num[newText[0]]*100)+num[newText[2]];
		}
		if(newText.length===4){
			result = (num[newText[0]]*100)+num[newText[2]]+num[newText[3]];
		}
	}
	return result;
}

module.exports = {
	getDayOfWeek,
	getDistanceBetweenXandY,
	getTextFromNumber,
	getNumberFromText
};
