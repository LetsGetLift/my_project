var  assert = require('chai').assert;

var moduleHW1testFunction = require ('../moduleHW1testFunction');

describe("input number of the day and get string value days of the week",function () {
    it("dayNumber = 2 string = Tuesday",function () {
        assert.equal(moduleHW1testFunction.getDayOfWeek(2),'Tuesday');
    });
	it("dayNumber = '2' string = Error",function () {
		assert.equal(moduleHW1testFunction.getDayOfWeek('2'),'Error');
	});
    it("dayNumber = 4 string = Tuesday",function () {
        assert.equal(moduleHW1testFunction.getDayOfWeek(4),'Thursday');
    });
    it("dayNumber = 1 string = Monday",function () {
        assert.equal(moduleHW1testFunction.getDayOfWeek(1),'Monday');
    });
    it("dayNumber = 3 string = Monday",function () {
        assert.equal(moduleHW1testFunction.getDayOfWeek(3),'Wednesday');
    });
    it("dayNumber = 5 string = Friday",function () {
        assert.equal(moduleHW1testFunction.getDayOfWeek(5),'Friday');
    });
    it("dayNumber = 6 string = Saturday",function () {
        assert.equal(moduleHW1testFunction.getDayOfWeek(6),'Saturday');
    });
    it("dayNumber = 7 string = Sunday",function () {
        assert.equal(moduleHW1testFunction.getDayOfWeek(7),'Sunday');
    });
});

describe("Calc distance between point in coordinates",function () {
	it("Ax=2 Ay=4 Bx=4 By=8",function () {
		assert.equal(moduleHW1testFunction.getDistanceBetweenXandY(2,4,4,8),4.47213595499958
		);
	});
	it("Ax=1 Ay=2 Bx=2 By=2",function () {
		assert.equal(moduleHW1testFunction.getDistanceBetweenXandY(1,2,2,2),1);
	});
});

describe("get number to tex",function () {

    it("number = 1 excepted = one",function () {
        assert.equal(moduleHW1testFunction.getTextFromNumber(1),"One");
    });
    it("number = 10 excepted = ten",function () {
        assert.equal(moduleHW1testFunction.getTextFromNumber(10),"Ten");
    });
    it("number = 19 excepted = Nineteen",function () {
        assert.equal(moduleHW1testFunction.getTextFromNumber(19),"Nineteen");
    });
    it("number = 29 excepted = Twenty Nine",function () {
        assert.equal(moduleHW1testFunction.getTextFromNumber(29),"Twenty Nine");
    });
    it("number = 20 excepted = Twenty",function () {
        assert.equal(moduleHW1testFunction.getTextFromNumber(20),"Twenty");
    });

    it("number = 100 excepted = One Hundred",function () {
        assert.equal(moduleHW1testFunction.getTextFromNumber(100),"One Hundreds");
    });
    it("number = 101 excepted = One Hundreds One",function () {
        assert.equal(moduleHW1testFunction.getTextFromNumber(101),"One Hundreds One");
    });

    it("number = 201 excepted = Two Hundreds One",function () {
        assert.equal(moduleHW1testFunction.getTextFromNumber(201),"Two Hundreds One");
    });

    it("number = 500 excepted = Five Hundred",function () {
        assert.equal(moduleHW1testFunction.getTextFromNumber(500),"Five Hundreds");
    });

    it("number = 999 excepted = Nine Hundreds Ninety Nine",function () {
        assert.equal(moduleHW1testFunction.getTextFromNumber(999),"Nine Hundreds Ninety Nine");
    });


});

describe("get number from text",function () {
    it("actual One expected 1", function () {
        assert.equal(moduleHW1testFunction.getNumberFromText("One"),1);
    });
    it("actual Ten expected 10", function () {
        assert.equal(moduleHW1testFunction.getNumberFromText("Ten"),10);
    });
    it("actual ELEVEN expected 11", function () {
        assert.equal(moduleHW1testFunction.getNumberFromText("ELEVEN"),11);
    });
    it("actual forty five expected 45", function () {
        assert.equal(moduleHW1testFunction.getNumberFromText("forty five"),45);
    });

    it("actual One hundred expected 100", function () {
        assert.equal(moduleHW1testFunction.getNumberFromText("one hundred"),100);
    });
    it("actual One hundred one expected 101", function () {
        assert.equal(moduleHW1testFunction.getNumberFromText("One hundred one"),101);
    });
    it("actual One hundred twenty one expected 121", function () {
        assert.equal(moduleHW1testFunction.getNumberFromText("One hundred twenty one"),121);
    });
    it("actual one hundred thirty eight expected 138", function () {
        assert.equal(moduleHW1testFunction.getNumberFromText("one hundred thirty eight"),138);
    });
    it("actual three hundred six expected 306", function () {
        assert.equal(moduleHW1testFunction.getNumberFromText("three hundred six"),306);
    });
    it("actual three hundred thirty nine expected 339", function () {
        assert.equal(moduleHW1testFunction.getNumberFromText("three hundred thirty nine"),339);
    });
    it("actual Nine hundred ninety nine expected 999", function () {
        assert.equal(moduleHW1testFunction.getNumberFromText("Nine hundred ninety nine"),999);
    });
    it("actual inputText===undefined expected 'Error' ", function () {
        assert.equal(moduleHW1testFunction.getNumberFromText(undefined),"Error");
    });
    it("actual inputText===null expected 'Error' ", function () {
        assert.equal(moduleHW1testFunction.getNumberFromText(null),"Error");
    });
    it("actual inputText!==string expected 'Error' ", function () {
        assert.equal(moduleHW1testFunction.getNumberFromText(99),"Error");
    });

});









