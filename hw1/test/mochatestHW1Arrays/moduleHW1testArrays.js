function findMin (unsortArray) {
        var min = 0;
        for (var i = 0; i < unsortArray.length; i++) {
            if (!unsortArray[i]) {
                return undefined;
            }
            if (min > unsortArray[i]) {
                min = unsortArray[i];
            }
        }
        return min;
    }

function findMax (unsortArray) {
        var max = unsortArray[0];
        for (var i = 0; i < unsortArray.length; i++) {
            if (!unsortArray[i]) {
                return;
            }
            if (max < unsortArray[i]) {
                max = unsortArray[i];
            }
        }
        return max;
    }

function findMinArray (array) {

        var min = array[0];
        var minIndex = array[0];
        for (var i = 1; i < array.length; i++) {

            if (min > array[i]) {
                min = array[i];
                minIndex = array.indexOf(array[i]);
            }
        }
        return minIndex;
    }

function findMaxArray (array) {
        var maxIndex = 0;
        for (var i = 1; i < array.length; i++) {

            if (array[maxIndex] < array[i]) {
                maxIndex = i;
            }
        }
        return maxIndex;
    }

function sumOfOddIndex (a) {
        var sum = 0;
        var smallvalues = [];
        for (var i = 0; i < a.length; i++) {
            if (a.indexOf(i) % 2 != 0) {
                smallvalues = a[i];
                sum += smallvalues;
            }
        }
        return sum;
    }

function sumOfOddNumber (a) {
        var smallvalues = [];
        var sum = 0;
        for (var i = 0; i < a.length; i++) {
            if (a[i] % 2 != 0) {
                smallvalues.push(a[i]);
            }
        }
        return smallvalues.length;
    }

function reverseArray (sortArray) {

sortArray.splice(3, 1, sortArray.splice(0, 1, sortArray[3])[0], sortArray.splice(1,1)[0]);
sortArray.shift(3);

return sortArray;
}
//    reverseArray: function(newNums) {
//
//        var result = [];
//        for (var i = 0; i < newNums.length; i += 4) {
//            result.push(newNums[i + 2], newNums[i + 3], newNums[i], newNums[i + 1]);
//        }
//        return result;
//    }

function bubbleSort  (unsortArray) {
    for(var i = 0; i < unsortArray.length; i++) {
        for (var k = 0; k < unsortArray.length; k++) {
            if (unsortArray[k] > unsortArray[k + 1]) {
                var temp = unsortArray[k];
                unsortArray[k] = unsortArray[k + 1];
                unsortArray[k + 1] = temp;
            }
        }
    }
    return unsortArray;
}

function sortSelect (sortArray){
    for (var i = 0; i < sortArray.length; i++){
        var min=i;
        for (var k = i+1; k < sortArray.length; k++){
            if (sortArray[min] > sortArray[k]){
                min = k;
            }
        }
        var res = sortArray[min];
        sortArray[min] = sortArray[ i ];
        sortArray[ i ] = res;
    }
    return sortArray;
}

function sortInsert (sortArray){
    for(var i=0;i<sortArray.length;i++){
        var res=sortArray[i];
        var k=i-1;
        while(k>=0 && sortArray[k]>res){
            sortArray[k + 1] = sortArray[k];
            k--;
        }
        sortArray[k+1]=res;
    }
    return sortArray;
    }

module.exports = {
    findMin,
    findMax,
    findMinArray,
    findMaxArray,
    sumOfOddIndex,
    sumOfOddNumber,
    reverseArray,
    bubbleSort,
    sortSelect,
    sortInsert
};












