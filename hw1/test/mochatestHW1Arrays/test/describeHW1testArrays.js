const  assert = require('chai').assert;
const moduleHW1testArrays = require ('../moduleHW1testArrays.js');

describe("findMin",function () {
    it("array = 1,2,3,-4 res = -4",function () {
        assert.equal(moduleHW1testArrays.findMin([1,2,3,-4]),-4);
    });
    it("array = ,1,2,3,-4 res = undefined",function () {
        assert.equal(moduleHW1testArrays.findMin([,1,2,3,-4]),undefined);
    });

});

describe("findMax",function () {
    it("array = 1, 2, 3, -4, 5,-9,1, res = -9",function () {
        assert.equal(moduleHW1testArrays.findMax([1, 2, 3, -4, 5,-9,1,]),5);
    });
    it("array = ,1, 2, 3, -4, 5,-9,1, res = undefined",function () {
        assert.equal(moduleHW1testArrays.findMax([,1, 2, 3, -4, 5,-9,1,]),undefined);
    });

});

describe("findMinArray",function () {
    it("should return 4 when array equals to [1, 2, 3, 4, 0, 1]",function () {
        assert.equal(moduleHW1testArrays.findMinArray([1, 2, 3, 4, 0, 1]), 4);
    });
    it("array = 1, 2, 3, -4, 5,-9,1, res = -9",function () {
        assert.equal(moduleHW1testArrays.findMinArray([1, 2, 3, -4, 5,-9,1,]),5);
    });
    it("array = , res = undefined",function () {
        assert.equal(moduleHW1testArrays.findMinArray([,]),undefined);
    });
});

describe("findMaxArray",function () {
    it("array = 1, 2, 3, -4, 5,-9,1, res = -9",function () {
        assert.equal(moduleHW1testArrays.findMaxArray([1, 2, 3, -4, 5,-9,1,]),4);
    });
});

describe("sumOfOddIndex",function () {
    it("array = 0,-1,2,3,4,5,6,7 res = 14",function () {
        assert.equal(moduleHW1testArrays.sumOfOddIndex([0,-1,2,3,4,5,6,7]),14);
    });
});

describe("sumOfOddNumber",function () {
    it("array = 0,-1,2,3,4,5,6,7 res = 4",function () {
        assert.equal(moduleHW1testArrays.sumOfOddNumber([0,-1,2,3,4,5,6,7]),4);
    });
});

describe("reverseArray",function () {
    it("array = 1234 res = 4312",function () {
        assert.deepEqual(moduleHW1testArrays.reverseArray([1,2,3,4]),[3,4,1,2]);
    });
});

describe("bubbleSort",function () {
    it("array = 1,7,8,4,3,5,9,6,2 res = 1,2,3,4,5,6,7,8,9",function () {
        assert.deepEqual(moduleHW1testArrays.bubbleSort([1,7,8,4,3,5,9,6,2]),[1,2,3,4,5,6,7,8,9]);
    });
});

describe("sortSelect",function () {
    it("array = 1,3,5,4,2 res = 1,2,3,4,5",function () {
        assert.deepEqual(moduleHW1testArrays.sortSelect([1,3,5,4,2]),[1,2,3,4,5]);
    });
});

describe("sortInsert",function () {
    it("array = 1,7,8,4,3,5,9,6,2 res = 1,2,3,4,5,6,7,8,9",function () {
        assert.deepEqual(moduleHW1testArrays.sortInsert([1,7,8,4,3,5,9,6,2]),[1,2,3,4,5,6,7,8,9]);
    });
});


