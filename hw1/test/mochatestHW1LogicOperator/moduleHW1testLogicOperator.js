function checkIsEven(a,b){
    if(typeof a!=='number' || typeof b!=='number'){
        return 'Error';
    }
    return a === 0 ? (a+b) : a%2 === 0 ? (a * b) : (a+b);
    }

function findCoordinates (x,y){
	if(typeof x!=='number' || typeof y!=='number'){
		return 'Error';
	}
	if(x>0 && y>0){
		return ("Координаты находятся на 1-й четверти");
	}
	else if(x<0 && y>0){
		return("Координаты находятся на 2-й четверти");
	}
	else if(x<0 && y<0){
		return("Координаты находятся на 3-й четверти");
	}
	else if(x>0 && y<0){
		return("Координаты находятся на 4-й четверти");
	}
	else if(x===0 && y>0 || y<0){
		return("Точка лежит на оси Y");
	}
	else if(y===0 && x<0 || x>0){
		return("Точка лежит на оси X");
	}
	else{
		return("Точки находятся в начале координат");
	}
}

function summ (a,b,c){
	if(typeof a!=='number' || typeof b!=='number' || typeof c!=='number' || (a<0 && b<0 && c<0)){
		return 'Error';
	}
	let summ=0;
	if(a>0){
		summ +=a;
	}
	if(b>0){
		summ +=b;
	}
	if(c>0){
		summ +=c;
	}
	return summ;
}

function getMax (a,b,c){
	if(typeof a!=='number' || typeof b!=='number' || typeof c!=='number'){
		return 'Error';
	}
	let summ =a+b+c;
	let mult =a*b*c;

	if(summ > mult){
		summ +=3;
		return summ;
	}else{
		mult +=3;
		return mult;
	}
}

function getMark(r){
	if(typeof r!=='number'){
		return 'Error';
	}
	return (
	    (r >= 0 && r <= 19) ? 'F' :
		(r >= 20 && r <= 39) ? 'E' :
		(r >= 40 && r <=59 ) ? 'D' :
		(r >= 60 && r <=74 ) ? 'C' :
		(r >= 75 && r <=89 ) ? 'B' :
		(r >= 90 && r<=100) ? 'A' :'Error')
}

module.exports = {
	checkIsEven,
	findCoordinates,
	summ,
	getMax,
	getMark
};

