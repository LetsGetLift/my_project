var  assert = require('chai').assert;

var moduleHW1testLogicOperator = require ('../moduleHW1testLogicOperator.js');

describe("checkIsEven", function() {
	it("а = 4 b= 3 res=12", function(){
		assert.equal(moduleHW1testLogicOperator.checkIsEven(4,3), 12);
	});
	it("a=1, b=8, res=9", function(){
		assert.equal(moduleHW1testLogicOperator.checkIsEven(1,8), 9);
	});
	it("a=-2, b=8, res=-16", function(){
		assert.equal(moduleHW1testLogicOperator.checkIsEven(-2,8),-16);
	});
	it("a=-2, b=-8, res=16", function(){
		assert.equal(moduleHW1testLogicOperator.checkIsEven(-2,-8),16);
	});
	it("a=0, b=8, res=8", function(){
		assert.equal(moduleHW1testLogicOperator.checkIsEven(0,8),8);
	});
	it("a='1', b='8', res=Error", function(){
		assert.equal(moduleHW1testLogicOperator.checkIsEven('2','8'),"Error");
	});
	it("a=null, b=undefined, res=Error", function(){
		assert.equal(moduleHW1testLogicOperator.checkIsEven(null,undefined),"Error");
	});
});

describe("find coordinates",function () {
    it("x>0 y>0 1-st quarter", function(){
        assert.equal(moduleHW1testLogicOperator.findCoordinates(4,3), "Координаты находятся на 1-й четверти");
    });

    it("x<0 y>0 2-nd quarter", function(){
        assert.equal(moduleHW1testLogicOperator.findCoordinates(-4,3), "Координаты находятся на 2-й четверти");
    });
    it("x<0 y<0 3-rd quarter", function(){
        assert.equal(moduleHW1testLogicOperator.findCoordinates(-4,-3), "Координаты находятся на 3-й четверти");
    });
    it("x>0 y<0 4-th quarter", function(){
        assert.equal(moduleHW1testLogicOperator.findCoordinates(4,-3), "Координаты находятся на 4-й четверти");
    });
    it("x=0 y!=0 y", function(){
        assert.equal(moduleHW1testLogicOperator.findCoordinates(0,-3), "Точка лежит на оси Y");
    });
    it("x!=0 y=0", function(){
        assert.equal(moduleHW1testLogicOperator.findCoordinates(3,0), "Точка лежит на оси X");
    });
    it("x=0 y=0 zero coordinates", function(){
        assert.equal(moduleHW1testLogicOperator.findCoordinates(0,0), "Точки находятся в начале координат");
    });
	it("x='0' y='0' zero coordinates", function(){
		assert.equal(moduleHW1testLogicOperator.findCoordinates('0','0'), "Error");
	});
});

describe("summ",function () {
		it('a=1 b=2 c=3 sum=6', function () {
			assert.equal(moduleHW1testLogicOperator.summ(1,2,3,),6);
		});
		it('a=-1 b=2 c=3 sum=5', function () {
			assert.equal(moduleHW1testLogicOperator.summ(-1,2,3,),5);
		});
		it('a=-1 b=-2 c=-3 Error', function () {
			assert.equal(moduleHW1testLogicOperator.summ(-1,-2,-3,),'Error');
		});
		it('a="1" b=2 c=3 Error', function () {
			assert.equal(moduleHW1testLogicOperator.summ("1",2,3,),'Error');
		});
});

describe("getMax",function () {
	it("if summ > mult a=1,b=1,c=1 res =6",function () {
		assert.equal(moduleHW1testLogicOperator.getMax(1,1,1),'6');
	});
    it("if mult > summ a=1,b=2,c=3 res =9",function () {
        assert.equal(moduleHW1testLogicOperator.getMax(1,2,3),'9');
    });
	it("if any argument !== number res='Error'",function () {
		assert.equal(moduleHW1testLogicOperator.getMax("2",2,3),'Error');
	});



});

describe("getMark",function () {
	it("a=0-19 res=F",function () {
		assert.equal(moduleHW1testLogicOperator.getMark(0, 19),'F');
	});
	it("a=20-39 res=E",function () {
		assert.equal(moduleHW1testLogicOperator.getMark(20, 39),'E');
	});
	it("a=40-59 res=E",function () {
		assert.equal(moduleHW1testLogicOperator.getMark(40, 59),'D');
	});
	it("a=60-74 res=C",function () {
		assert.equal(moduleHW1testLogicOperator.getMark(60, 74),'C');
	});
	it("a=75-89 res=B",function () {
		assert.equal(moduleHW1testLogicOperator.getMark(75, 89),'B');
	});
	it("a=90-100 res=A",function () {
		assert.equal(moduleHW1testLogicOperator.getMark(90, 100),'A');
	});
	it("a=-100-101 res=Error",function () {
		assert.equal(moduleHW1testLogicOperator.getMark(-100,101),'Error');
	});
	it("a='40'-'59' res=Error",function () {
		assert.equal(moduleHW1testLogicOperator.getMark('40','59'),'Error');
	});
		it("a=null-undefined res=Error",function () {
		assert.equal(moduleHW1testLogicOperator.getMark(null,undefined),'Error');
	});
});
