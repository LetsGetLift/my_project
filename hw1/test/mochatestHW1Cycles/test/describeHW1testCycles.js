var  assert = require('chai').assert;

var moduleHW1testCycles = require ('../moduleHW1testCycles.js');

describe("tickets",function () {
    it("YES",function () {
        assert.equal(moduleHW1testCycles.tickets([[25,25,25,100,25,25,25,100,25,25,50,100,25,50,50,25]]),"YES");
    });
});



//
// describe("findEven",function () {
// 	it("n=1-99",function () {
// 		assert.equal(moduleHW1testCycles.findEven(99),[2450,49]);
// 	});
// 	it("n=-99",function () {
// 		assert.equal(moduleHW1testCycles.findEven(-99),'Error');
// 	});
// 	it("n='99'",function () {
// 		assert.equal(moduleHW1testCycles.findEven('99'),'Error');
// 	});
// });
//
// describe("simpleOrNot", function () {
// 	it("a=5 res Simple",function () {
// 		assert.equal(moduleHW1testCycles.simpleOrNot(5),'Simple');
// 	});
// 	it("a=9 res Compound",function () {
// 		assert.equal(moduleHW1testCycles.simpleOrNot(9),'Compound');
// 	});
// 	it("a='9' res Error",function () {
// 		assert.equal(moduleHW1testCycles.simpleOrNot('9'),'Error');
// 	});
// 	it("a=-9 res Error",function () {
// 		assert.equal(moduleHW1testCycles.simpleOrNot(-9),'Error');
// 	});
// });
//
// describe("findDegree",function () {
//     it("degree=24 res=4",function () {
//         assert.equal(moduleHW1testCycles.findDegree(24),4);
//     });
//     it("degree=26 res=5",function () {
//         assert.equal(moduleHW1testCycles.findDegree(26),5);
//     });
//     it("degree=-24 res=Error",function () {
//         assert.equal(moduleHW1testCycles.findDegree(-24),'Error');
//     });
//     it("degree=0 res=Error",function () {
//         assert.equal(moduleHW1testCycles.findDegree(0),'Error');
//     });
//     it("degree='0' res=Error",function () {
//         assert.equal(moduleHW1testCycles.findDegree('0'),'Error');
//     });
// });
//
// describe("calcFactorial",function () {
//     it("number=6 res=720",function () {
//         assert.equal(moduleHW1testCycles.calcFactorial(6),720);
//     });
//     it("number=0 res=1",function () {
//         assert.equal(moduleHW1testCycles.calcFactorial(0),1);
//     });
//     it("number=-4 res=Error",function () {
//         assert.equal(moduleHW1testCycles.calcFactorial(-4),'Error');
//     });
//     it("number='-4' res=Error",function () {
//         assert.equal(moduleHW1testCycles.calcFactorial('-4'),'Error');
//     });
//     it("number='-4' res=Error",function () {
//         assert.equal(moduleHW1testCycles.calcFactorial('-4'),'Error');
//     });
//
//     it("number=50 res=30414093201713378043612608166064768844377641568960512000000000000",function () {
//         assert.equal(moduleHW1testCycles.calcFactorial(50),30414093201713378043612608166064768844377641568960512000000000000);
//     });
// });
//
// describe("summOfDigits",function () {
//     it("digit=123 res=6", function () {
//         assert.equal(moduleHW1testCycles.summOfDigits(123),6);
//     });
//     it("digit=-123 res=-6", function () {
//         assert.equal(moduleHW1testCycles.summOfDigits(-123),-6);
//     });
//     it("digit='-123' res=Error", function () {
//         assert.equal(moduleHW1testCycles.summOfDigits('-123'),'Error');
//     });
// });
//
// describe("reversDigit",function () {
//     it("digit = 123 res= 321",function () {
//         assert.equal(moduleHW1testCycles.reversDigit(123),321);
//     });
//     it("digit = -123 res= -321",function () {
//         assert.equal(moduleHW1testCycles.reversDigit(-123),-321);
//     });
//     it("digit = '-123' res= 'Error'",function () {
//         assert.equal(moduleHW1testCycles.reversDigit('-123'),'Error');
//     });
// });