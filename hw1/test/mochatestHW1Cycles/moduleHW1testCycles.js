function findEven(n) {
    var summ = 0;
    var numberOfEven = 0;
    if (typeof n !== 'number' || n < 0) {
        return "Error";
    }
    numbOfCycles:
        for (var i = 1; i <= n; i++) {
            if (i % 2 !== 0) {
                continue numbOfCycles;
            }
            summ += i;
            numberOfEven += 1;
        }
    return [summ, numberOfEven];
}

function simpleOrNot(a) {
    if (typeof a !== 'number' || a < 0) {
        return "Error";
    }
    var isSimple = true;
    for (var i = 2; i < a; i++) {
        if (a <= 1 || a % i === 0) {
            isSimple = false;
        }
    }
    return (a > 1 && isSimple) ? "Simple" : "Compound";
}

function findDegree(degree) {
    var left = 1;
    var right = degree;
    var q = 0;

    if (typeof degree !== 'number' || degree <= 0) {
        return 'Error';
    }

    for (; ;) {
        var mid = Math.floor((left + right) / 2);
        if (q === mid) {
            return mid;
        }
        var multiplyMid = mid * mid;
        if (degree === multiplyMid) {
            return mid;
        }
        if (degree < multiplyMid) {
            right = mid;
        } else {
            left = mid;
        }
        q = mid;
    }
}

function calcFactorial(factorial) {
    if (typeof factorial !== 'number' || factorial < 0) {
        return 'Error';
    }
    var res = 1;
    for (var i = 2; i <= factorial; i++) {
        res = res * i;
    }
    return res;
}

function summOfDigits(digit) {
    if (typeof digit !== 'number') {
        return "Error";
    }
    var restOfDivision;
    var sum = 0;
    while (digit) {

        if (digit > 0) {
            restOfDivision = digit % 10;
            digit = (digit - restOfDivision) / 10;
            sum += restOfDivision;
        } else {
            restOfDivision = digit % 10;
            digit = (digit - restOfDivision) / 10;
            sum += restOfDivision;
        }
    }
    return sum;
}

function reversDigit(digit) {

    if (typeof digit !== 'number') {
        return "Error";
    }
    var restOfDivision;
    var sum = 0;

    while (digit > 0) {
        restOfDivision = digit % 10;
        sum = sum + (restOfDivision / 10);
        digit = (digit - restOfDivision) / 10;
        sum = sum * 10;
    }
    return sum;
}
function tickets(peopleInLine){
    var man25 = 0, man50 = 0;
    for(var i = 0;i<peopleInLine.length;i++){
        if(peopleInLine[i] == 25){
            man25 += 1;
        }
        if(peopleInLine[i] == 50){
            man25 -= 1; man50 += 1;
        }
        if(peopleInLine[i] == 100){
            if(man50 == 0 && man25 >= 3){
                man25 -= 3;
            }else{
                man25 -= 1; man50 -= 1;
            }
        }
    }
    if(man25 < 0 || man50 < 0){
        return 'NO';
    }
    return 'YES';
}


module.exports = {
    findEven,
    simpleOrNot,
    findDegree,
    calcFactorial,
    summOfDigits,
    reversDigit,
    tickets
};