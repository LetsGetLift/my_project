let canvas = document.getElementById("myCanvas");

let ctx = canvas.getContext("2d");

let CreateBall = function(){
    this.x =getRandomPositionX();
    this.y =getRandomPositionY();
    this.radius = getRandomRadius();
    this.dx =10;
    this.dy = -10;
    this.color=getRandomColor();
};
CreateBall.prototype.draw = function () {
    // ctx.clearRect(0, 0, canvas.width, canvas.height);
    if(this.x + this.dx > canvas.width-this.radius || this.x + this.dx < this.radius) {
        this.dx = -this.dx;
    }
    if(this.y + this.dy > canvas.height-this.radius || this.y + this.dy < this.radius) {
        this.dy = -this.dy;
    }
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
    ctx.fill();
    ctx.fillStyle = this.color;
    this.x += this.dx;
    this.y += this.dy;
};


let arr = [];

drawBall = (e) =>{
    this.x = e.offsetX;
    this.y = e.offsetY;
    let ball = new CreateBall();
    arr.push(ball);
};
setInterval(()=>armyBalls(arr),30)


function armyBalls(arr) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    for (let i = 0; i < arr.length; i++) {
        let balls = arr[i];
        balls.draw();
    }
}

canvas.addEventListener("click", drawBall);


function getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function getRandomRadius() {
    return Math.floor(Math.random() * 50)+10;
}
function getRandomPositionX() {
    return Math.floor(Math.random() * 800)+40;
}
function getRandomPositionY() {
    return Math.floor(Math.random() * 400)+40;
}


