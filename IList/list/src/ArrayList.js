function IList() {}

IList.prototype.init = () => {}; // метод для инициализации var array = new ArrayList(); array.init([1,2,3,4,5])
IList.prototype.toString = () => {}; // +
IList.prototype.getSize = () => {}; // +
IList.prototype.push = (value) => {}; // +
IList.prototype.pop = () => {};
IList.prototype.shift = () => {};
IList.prototype.unshift = (value) => {};
IList.prototype.slice = (start, end) => {};
IList.prototype.splice = (start, numberToSplice, ...elements) => {};
IList.prototype.sort = (comparator) => {}; // comparator ==> callback
IList.prototype.get = (index) => {}; // get by index
IList.prototype.set = (index, element) => {}; // set element by index


const ArrayList = function () {
    IList.apply(this, arguments);
    this.array = [];
};


ArrayList.prototype = Object.create(IList.prototype);
ArrayList.prototype.constructor = ArrayList;

ArrayList.prototype.init = function (initialArray) {
    for(let i = 0; i < arguments.length; i++){
        this.array.push(arguments[i]);
    }
    return this.array;
};
ArrayList.prototype.toString = function () {
    var str = '';
    for(var i = 0; i < this.array.length; i ++) {
        if(this.array.length-1 === i) {
            str += this.array[i];
        }else {
            str += this.array[i] + ',';
        }
    }
    return str;
};

ArrayList.prototype.getSize = function () {
    return this.array.length;
};

ArrayList.prototype.push = function (arg) {
   this.array[this.array.length] = arg;
   return this.array;
};
ArrayList.prototype.pop = function () {
    var lastElementInArray = this.array[this.array.length - 1];
    this.array.length = this.array.length - 1;
    return lastElementInArray;
};

ArrayList.prototype.shift = function() {
    var firstElementInArray = this.array[0];
    for (var i = 0; i < this.array.length; i++) {
        this.array[i] = this.array[i + 1];
    }
    this.array.length = this.array.length - 1;
    return firstElementInArray;
};
ArrayList.prototype.unshift = function (arg) {
    this.array.length = this.array.length + 1;
    for (var i = this.array.length - 1; i > 0; i--) {
        this.array[i] = this.array[i - 1];
        console.log(this.array[i]);
    }
    this.array[0] = arg;
    return this.array.length;
};
ArrayList.prototype.slice = function (start,end) {
    var sliceArray=[];
    var spliceEnd = end || this.array.length;
    for (var i = start; i < spliceEnd; i++) {
        sliceArray.push(this.array[i]);
    }
    return sliceArray;
};

ArrayList.prototype.splice = function (start,delElem,element) {

};




var initialArray = new ArrayList();
console.log(initialArray.init("Hello","Allo",1,2,3,4));
// console.log(initialArray.toString());
// console.log(initialArray.getSize());
// console.log(initialArray.push('new element'));
// console.log(initialArray.pop());
// console.log(initialArray.shift());
// console.log(initialArray.unshift(2));
// console.log(initialArray.slice(2,3));










