var inputMin = document.querySelector("#inputMin");
var inputMax = document.querySelector("#inputMax");
var inputResult = document.querySelector("#inputResult");
var generate = document.querySelector("#generate");
var reset = document.querySelector("#reset").addEventListener("click", getReset);
var result = [];
generate.addEventListener("click", getRandomNumber);
inputMin.addEventListener("onkeyup", disableGenerateIfNoInput);


function generateRandomDigits(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

function getRandomNumber() {

    var min = Number(inputMin.value);
    var max = Number(inputMax.value) + 1;

    if (result.length === (max - min)) {
        inputResult.value = "The numbers are over";
        return;
    }

    var randomDigits = generateRandomDigits(min, max);

    if (checkDigitsInResult(randomDigits, result)) {
        getRandomNumber(min, max);
    } else {
        result.push(randomDigits);
        inputResult.value = randomDigits;
    }

}

function checkDigitsInResult(value, array) {
    for (var isInArray = 0; isInArray < array.length; isInArray++) {
        if (array[isInArray] === value) {
            return true;
        } else {
            return false;
        }
    }
}

function getReset() {
    result = [];
    inputMin.value = "";
    inputMax.value = "";
    inputResult.value = "";
}

function disableGenerateIfNoInput(){
    if (inputMin.length !== 0 && inputMax.length !== 0) {
        generate.classList.toggle("active");
    }
}


