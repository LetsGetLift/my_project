package HW1_Java;
import java.util.Scanner;

class getDayOfWeek {
    static void getDayOfWeek(int dayNumber) {
        switch (dayNumber) {
            case 1:
                System.out.println ("Monday");
                break;
            case 2:
                System.out.println ("Tuesday");
                break;
            case 3:
                System.out.println ("Wednesday");
                break;
            case 4:
                System.out.println ("Thursday");
                break;
            case 5:
                System.out.println ("Friday");
                break;
            case 6:
                System.out.println ("Saturday");
                break;
            case 7:
                System.out.println ("Sunday");
                break;
            default:
                System.out.println ("Error");
                break;
        }
    }

    public static void main(String[] args) {
        getDayOfWeek (7);
    }
}
class getDistanceBetweenXandY {
    static void getDistanceBetweenXandY(double pointAx, double pointAy, double pointBx, double pointBy) {

        double calc = Math.sqrt((Math.pow((pointBx-pointAx),2)+Math.pow((pointBy-pointAy),2)));
        System.out.println (calc);
    }

    public static void main(String[] args) {
        getDistanceBetweenXandY (1,4,1,1);
    }
}
class NumberWriter {
    public static final String[] num = {"Zero","One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight",
            "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen",
            "Seventeen", "Eighteen", "Nineteen"};
    public static final String[] tens = {"","ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while ( true ) {
            System.out.print("Write number from 0 to 999: ");
            if ( ! scanner.hasNextInt() )
                break;
            int number = scanner.nextInt();
            if ( number < 0 || number > 1000 ) {
                System.out.println("Try again");
                continue;
            }

            if ( number < 20 )
                System.out.println(num[number]);
            else if ( number < 100 ) {
                int high = number / 10;//9
                int low = number % 10;
                String text = tens[high];
                if ( low != 0 )
                    text = text + " " + num[low];
                System.out.println(text);
            }
            else if ( number > 100 ) {
                int high = number / 100;//1.25
                int low = number % 100;//25
                int modulo = low % 10;//5
                int modulo1=(low-modulo)/10;
                String text = num[high]+" "+"hundred";
                if ( low != 0 )
                    text = text + " " + tens[modulo1]+" " +num[modulo];
                System.out.println(text);
            }
            else
                System.out.println(tens[0]);
        }

        scanner.close();
    }
}




