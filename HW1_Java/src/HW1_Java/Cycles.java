package HW1_Java;

class findEven {
    public static void main(String[] args) {
        int numberOfEven = 0;
        int sum = 0;

        for (int i = 2; i < 100; i++) {
            if (i % 2 == 0) {
                numberOfEven++;
                sum += i;
            }
        }
        System.out.println ("sum of even digits: " + sum);
        System.out.println ("number of even digits: " + numberOfEven);
    }
}
class isSimple {

    public static void main(String[] args) {
        int a = 1;
        boolean isSimple = true;
        int i;
        for (i = 2; i < a; i++) {
            if (a % i == 0) {
                isSimple = false;
                break;
            }
        }
        if (a > 1 && isSimple) {
            System.out.println ("simple");
        } else {
            System.out.println ("compound");
        }
    }
}
class FindSquareRoot {

    public static void main(String[] args) {
        int inputNumber = 24;
        System.out.println (findSquareRoot (1, inputNumber, inputNumber));
    }

    public static int findSquareRoot(int left, int right, int inputNumber) {

        if (inputNumber == 0 || inputNumber == 1) {
            return inputNumber;
        }
        int mid = (left + right) / 2;
        if (mid * mid <= inputNumber && (mid + 1) * (mid + 1) > inputNumber) {
            return mid;
        }
        if (mid * mid < inputNumber) {
            return findSquareRoot (mid + 1, right, inputNumber);
        } else {
            return findSquareRoot (left, mid - 1, inputNumber);
        }

    }
}
class calcFactorial{
    static int calculateFactorial(int n){
        int result = 1;
        for (int i = 1; i <=n; i ++){
            result = result*i;
        }
        return result;
    }

    public static void main(String[] args){
        System.out.println(calculateFactorial(5));
    }
}
class calcSumOfDigits{
    public static void main(String[] args) {
        int num = 891;
        int sum = 0;
        while (num > 0) {
            sum = sum + num % 10;
            num = num / 10;
        }
        System.out.println(sum);
    }
}
class reverseDigit{

        public static void main(String[] args){
            int number = 9876;
            int reverse = 0;
            while(number > 0){
                int dig = number%10;
                reverse = reverse*10+dig;
                number /= 10;
            }
            System.out.println(reverse);
        }
}




