package HW1_Java;
import java.util.Arrays;

class FindMin {
    public static void main(String args[]) {
        int a[] = {1,2,3,4,5,-8,2,3,-7};
        int min = a[0];
        for(int x: a) {
            if(x < min){
                min = x;
            }
        }
        System.out.println("Min element: " + min);
    }
}
class FindMax {
    public static void main(String args[]) {
        int a[] = {1,2,3,4,5,-8,2,3,-7};
        int max = a[0];
        for(int x: a) {
            if(x > max){
                max = x;
            }
        }
        System.out.println("Max element: " + max);
    }
}
class findMaxIndexArray {
    public static void main(String args[]) {
        int array[] = {1, 10, 3, 14, 5, -8, 2, 3, -7};
        int maxIndex = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > array[maxIndex]) {
                maxIndex = i;
            }
        }
        System.out.println ("Max index: " + maxIndex);
    }
}
class findMinIndexArray {
    public static void main(String args[]) {
        int array[] = {1, 10, 3, 14, 5, -8, 2, 3, -7};
        int minIndex = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] < array[minIndex]) {
                minIndex = i;
            }
        }
        System.out.println ("Min index: " + minIndex);
    }
}
class calcSumOfOddIndex {
    public static void main(String[] args) {
        int [] arr =  {1, 2, 3, 7, 5};
        int summ = 0;
        for (int i = 0; i < arr.length; i++) {
            if (i % 2!= 0) {
                summ += arr[i];
            }
        }
        System.out.println(summ);
    }
}
class ArrayReverseExample{
    static void reverseArray(int inputArray[])
    {

        int temp;

        for (int i = 0; i < inputArray.length/2; i++)
        {
            temp = inputArray[i];

            inputArray[i] = inputArray[inputArray.length-1-i];

            inputArray[inputArray.length-1-i] = temp;
        }

        System.out.println("Array After Reverse : "+Arrays.toString(inputArray));
    }

    public static void main(String[] args)
    {
        reverseArray(new int[]{1, 2, 3, 4, 5});
    }
}
class calcSumOfOddElement {
    public static void main(String[] args) {
        int [] arr =  {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};//0.1.2.3.4
        int summ = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 != 0) {
                summ += 1;
            }
        }
        System.out.println(summ);
    }
}
